class TestParser
prechigh
  nonassoc UMINUS
  left	   '*' '/'
  left	   '+' '-'
preclow
rule
program	: stmt_list
	{
		result = RootNode.new(val[0])
	}

stmt_list :
	    { result = [] }
	  | stmt_list stmt EOL
	    { result.push val[1] }
	  | stmt_list EOL

stmt	: expr
        | assign
        | IDENT realprim
          {
	    result = FuncallNode.new(@fname, val[0][0], val[0][1], [val[1]])
	  }
	| if_stmt
	| while_stmt
        | defun

defun   : DEF IDENT param EOL stmt_list END
	  {
	    result = DefNode.new(@fname, val[0][0], val[1][1],
                                 Function.new(@fname, val[0][0], val[2], val[4]))
	  }

param   : '(' name_list ')'
	  { result = val[1] }
  	| '(' ')'
	  { result = [] }
  	| 
	  { result = [] }

name_list: IDENT
	  { result = [val[0][1]] }
  	| name_list ',' IDENT
	  { result.push val[2][1] }

if_stmt : IF stmt THEN EOL stmt_list else_stmt END
	  { 
	    result = IfNode.new(@fname, val[0][0], val[1], val[4], val[5])
	  }

else_stmt: ELSE EOL stmt_list
	   {
	     result = val[2]
	   }

while_stmt: WHILE stmt DO EOL stmt_list END
            {
	      result = WhileNode.new(@fname, val[0][0], val[1], val[4])
	    }

funcall	: IDENT '(' args ')'
	  {
		result = FuncallNode.new(@fname, val[0][0], val[0][1], val[2])
	  }
	| IDENT '(' ')'
	  {
		result = FuncallNode.new(@fname, val[0][0], val[0][1], [])
	  }

args	: expr
	  {
		result = val
	  }
	| args ',' expr
	  {
		result.push val[2]
	  }

assign	: IDENT '=' expr
	  {
	    result = AssignNode.new(@fname, val[0][0], val[0][1], val[2])
	  }

expr	: expr '+' expr
	  {
	    result = FuncallNode.new(@fname, val[0].lineno, '+', [val[0], val[2]])
	  }
	| expr '-' expr
	  {
	    result = FuncallNode.new(@fname, val[0].lineno, '-', [val[0], val[2]])
	  }
	| expr '*' expr
	  {
	    result = FuncallNode.new(@fname, val[0].lineno, '*', [val[0], val[2]])
	  }
	| expr '/' expr
	  {
	    result = FuncallNode.new(@fname, val[0].lineno, '/', [val[0], val[2]])
	  }
	| primary
	

primary	: realprim
	| '(' expr ')'
	  { result = val[1] }
	| '-' primary =UMINUS
	  { result = FuncallNode.new(@fname, val[0].lineno, '-@', [val[1]]) }

realprim: NIL
	  { result = NilNode.new(@fname, *val[0]) }
	| TRUE
	  { result = TrueNode.new(@fname, *val[0]) }
	| FALSE
	  { result = FalseNode.new(@fname, *val[0]) }
        | IDENT
	  {
	    result = VarRefNode.new(@fname, val[0][0], val[0][1])
	  }
	| NUMBER
	  {
	    result = LiteralNode.new(@fname, *val[0])
 	  }
	| STRING
	  {
	    result = StringNode.new(@fname, *val[0])
	  }
        | funcall

end

---- inner

require 'pp'

RESERVED = {
  'if'	 => :IF,
  'else' => :ELSE,
  'while' => :WHILE,
  'then' => :THEN,
  'do'	 => :DO,
  'end'	 => :END,
  'nil' => :NIL,
  'false' => :FALSE,
  'true' => :TRUE
}

def parse(f, fname)
  @q = []
  @fname = fname
  @lineno = 1

  f.each do |line|
    line.strip!
    until line.empty?
      case line
      when /\A\s+/, /\A\#.*\z/
        ;
      when /\A[a-zA-Z_]\w*/
        word = $&
        @q.push([RESERVED[word]||:IDENT, [@lineno, word.intern]])
      when /\A\d+/
        @q.push([:NUMBER, [@lineno, $&.to_i]])
      when /\A['"](?:[^"'\\]+|\\.)*['"]/
        @q.push([:STRING, [@lineno, eval($&)]])
      when /\A./
        @q.push([$&, [@lineno, $&]])
      else
        raise RuntimeError, 'must not happen'
      end
      line = $'
    end
    @q.push([:EOL, [@lineno, nil]])
  end
  @q.push([false, '$'])
  @yydebug = true
  do_parse
end

def next_token
  @q.shift
end

def on_error(t, v, vstack)
  if v
    line = v[0]
    v = v[1]
  else
   line = 'last'
  end
  raise Racc::ParseError, "#{@fname}:#{line}: syntax error on #{v.inspect}"
end

---- footer

class IntpError < StandardError; end

class Node
  def initialize(fname, linno)
    @filename = fname
    @lineno = lineno
  end

  attr :filename
  attr :lineno

  def exec_list(intp, nodes)
    v = nil
    nodes.each{|i| v = i.evaluate(intp) }
    v
  end

  def intp_error!(msg)
    raise IntpError, "in #{filename}:#{lineno}: #{msg}"
  end

  def inspect
    "#{type.name}/#{lineno}"
  end
end

class RootNode < Node
  def initialize(tree)
    super nil, nil
    @tree = tree
  end

  def evaluate
    exec_list Intp.new, @tree
  end
end

class FuncallNode < Node
  def initialize(fname, lineno, func, args)
    super fname, lineno
    @func = func
    @args = args
  end

  def evaluate
    arg = @args.collect{|i| i.evaluate}
    recv = Object.new
    if recv.respond_to?(@func, true)
      ;
    elsif arg[0] && arg[0].respond_to?(@func)
      recv = arg.shift
    else
      intp_error! "undefined method #{@func.id2name}"
    end
    recv.send(@func, *arg)
  end
end

class IfNode < Node
  def initialize(fname, lineno, cond, tstmt, fstmt)
    super fname, lineno
    @condition = cond
    @tstmt = tstmt 
    @fstmt = fstmt
  end

  def evaluate
    if @condition.evaluate
      exec_list @tstmt
    else
      exec_list @fstmt if @fstmt
    end
  end
end

class WhileNode < Node
  def initialize(fname, lineno, cond, body)
    super fname, lineno
    @condition = cond
    @body = body
  end

  def evaluate
    while @condition.evaluate != 0 do
      exec_list @body
    end
  end
end

class AssignNode < Node
  def initialize(fname, lineno, vtable, vname, val)
    super fname, lineno
    @vtable = vtable
    @vname = vname
    @val = val
  end

  def evaluate
    @vtable[@vname] = @val.evaluate
  end
end

class VarRefNode < Node
  def initialize(fname, lineno, vname)
    super fname, lineno
    @vname = vname
  end

  def evaluate(intp)
    if intp.frame.lvar? @vname
      intp.frame[@vname]
    else
      intp.call_function_or(@vname, []) do
        intp_error! "unknown method or local variable #{@vname.id2name}"
      end
    end
  end
end

class StringNode < Node
  def initialize(fname, lineno, str)
    super fname, lineno
    @val = str
  end

  def evaluate
    @val.dup
  end
end

class LiteralNode < Node
  def initialize(fname, lineno, val)
    super fname, lineno
    @val = val
  end

  def evaluate
    @val
  end
end

class NilNode < Node
  def initialize(fname, lineno, val)
    super fname, lineno
  end

  def evaluate
    nil
  end
end

class TrueNode < Node
  def initialize(fname, lineno, val)
    super fname, lineno
  end

  def evaluate
    true
  end
end

class FalseNode < Node
  def initialize(fname, lineno, val)
    super fname, lineno
  end

  def evaluate
    false
  end
end

class Intp
  def Initialize
    @ftab = {}
    @obj = Object.new
    @stack = []
    @stack.push(IntpFrame.new('(Toplevel)'))
  end
  
  def frame
    @stack.last
  end

  def define_function(fname, node)
    if @ftab.key? fname
      raise IntpError, "function #{fname.id2name} defined twice"
    end
    @ftab[fname] = node
  end

  def call_function_or(fname, args)
    call_intp_function_or(fname, args) do
      call_ruby_toplevel_or(fname, args) do
        yield
      end
    end
  end

  def call_intp_function_or(fname, args)
    if func = @ftab[fname]
      frame = IntpFrame.new fname
      @stack.push frame
      func.call self, frame, args
      @stack.pop
    else
      yield
    end
  end

  def call_ruby_toplevel_or(fname, args)
    if @obj.respond_to? fname, true
      @obj.send fname, *args
    else
      yield
    end
  end
end

class IntpFrame
  def initialize(fname)
    @fname = fname
    @lvars = {}
  end

  attr :fname
  
  def lvar?(name)
    @lvars.key? name
  end

  def [](key)
    @lvars[key]
  end

  def []=(key, val)
    @lvars[key] = val
  end
end

class DefNode < Node
  def initialize(file, nineno, fname, func)
    super file, lineno
    @funcname = fname
    @funcobj = func
  end

  def evaluate(intp)
    intp.define_function @funcname, @funcobj
  end
end

class Function < Node
  def initialize(file, lineno, param, body)
    super file, lineno
    @param = param
    @body = body
  end

  def call(intp, frame, args)
    unless args.size = @params.size
      raise(IntpArgumentError, 'wrong # of arg for %S() (%d for %d)' % [frame.fname, args.size, @params.size])
    end
    args.each_with_index do |v,i|
      frame[@params[i]] = v
    end
    exec_list intp, @body
  end
end

class FuncallNode < Node
  def initialize(file, lineno, func, args)
    super file,lineno
    @funcname = func
    @args = args
  end

  def evaluate(intp)
    arg = @args.collect{|i| i.evaluate intp }

    begin
      intp.call_function_or(@funcname, arg) do
        if arg.empty? or !arg[0].respond_to? @funcname
          intp_error! "undefined function #{@funcname.id2name}"
        end
      end
    rescue INtpArgumentError, ArgumentError
      intp_error! $!.message
    end
  end
end

begin
  tree = nil
  if ARGV[0]
    File.open(ARGV[0]) do |f|
      tree = TestParser.new.parse(f, ARGV[0])
    end
  else
    tree = TestParser.new.parse($stdin, '-')
  end

  tree.evaluate
rescue Racc::ParseError, IntpError, Errno::ENOENT
  $stderr.puts "#{File.basename($0)}: #{$!}"
  exit 1
end

